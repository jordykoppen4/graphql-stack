import { verify } from 'jsonwebtoken';

const decodeToken = (req, res, next) => {
  const { token } = req.cookies;

  if (token) {
    const { userId } = verify(token, process.env.JWT_SECRET);

    req.userId = userId;
  }

  next();
};

export default decodeToken;
