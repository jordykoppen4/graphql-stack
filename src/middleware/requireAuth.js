const requireAuth = next => (parent, args, ctx, info) => {
  if (!ctx.req.user) {
    throw new Error('Unauthenticated');
  }

  return next(parent, args, ctx, info);
};

export default requireAuth;
