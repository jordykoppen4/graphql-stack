import { prisma } from '../generated/prisma-client';

const populateUser = async (req, res, next) => {
  if (!req.userId) return next();

  const user = await prisma.user({ id: req.userId });
  req.user = user;

  next();
};

export default populateUser;
