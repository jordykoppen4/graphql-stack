import server from './server';

const config = {
  port: process.env.PORT,
};

server.listen(config, data =>
  console.log(`🚀  Server ready at http://localhost:${config.port}`),
);
