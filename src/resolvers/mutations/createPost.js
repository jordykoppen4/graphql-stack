import { prisma } from '../../generated/prisma-client';
import requireAuth from '../../middleware/requireAuth';

const createPost = (parent, args) => prisma.createPost(args.data);

export default requireAuth(createPost);
