import bcrypt from 'bcryptjs';
import { prisma } from '../../generated/prisma-client';

const createUser = async (parent, args, ctx, info) => {
  const { password, ...otherArgs } = args.data;
  const hashedPassword = await bcrypt.hash(password, 10);

  const data = {
    password: hashedPassword,
    ...otherArgs
  };

  const user = await prisma.createUser(data, info);

  return user;
};

export default createUser;
