import { prisma } from '../../generated/prisma-client';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';

const authenticate = async (parent, { data }, ctx, info) => {
  const { email, password } = data;
  const user = await prisma.user({ email: email });

  if (!user) {
    throw new Error('No such user found with this email address');
  }

  const passwordValid = bcrypt.compare(password, user.password);

  if (!passwordValid) {
    throw new Error('Invalid password');
  }

  const token = jwt.sign({ userId: user.id }, process.env.JWT_SECRET);

  ctx.res.cookie('token', token, {
    httpOnly: true,
    maxAge: 1000 * 60 * 60 * 24 * 365,
  });

  return user;
};

export default authenticate;
