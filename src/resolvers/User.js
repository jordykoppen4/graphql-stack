import { prisma } from '../generated/prisma-client';

const User = {
  posts: ({ id }) => prisma.user({ id }).posts(),
};

export default User;
