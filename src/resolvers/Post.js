import { prisma } from '../generated/prisma-client';

const Post = {
  author: ({ id }) => prisma.post({ id }).author(),
};

export default Post;
