import createUser from './mutations/createUser';
import createPost from './mutations/createPost';
import authenticate from './mutations/authenticate';

const Mutation = {
  // Public
  createUser,
  authenticate,
  // Authenticated
  createPost,
};

export default Mutation;
