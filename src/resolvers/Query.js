import { prisma } from '../generated/prisma-client';
import requireAuth from '../middleware/requireAuth';

const Query = {
  // Public
  posts: () => prisma.posts({ where: { published: true } }),
  drafts: () => prisma.posts({ where: { published: false } }),
  // Authenticated
  me: requireAuth((_, args, ctx) => prisma.user({ id: ctx.req.user.id })),
  user: requireAuth((_, args) => prisma.user(args.where)),
  users: requireAuth((_, args) => prisma.users(args))
};

export default Query;
