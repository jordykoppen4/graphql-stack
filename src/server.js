import { ApolloServer, makeExecutableSchema } from 'apollo-server-express';
import { importSchema } from 'graphql-import';
import Query from './resolvers/Query';
import cookieParser from 'cookie-parser';
import express from 'express';
import Mutation from './resolvers/Mutation';
import User from './resolvers/User';
import Post from './resolvers/Post';
import db from './db';
import populateUser from './middleware/populateUser';
import decodeToken from './middleware/decodeToken';

const typeDefs = importSchema('./src/schema.graphql');
const resolvers = {
  Query,
  Mutation,
  User,
  Post
};
const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
  resolverValidationOptions: {
    requireResolversForResolveType: false
  }
});

const playground = {
  settings: {
    'request.credentials': 'include'
  }
};

const serverConfig = {
  schema,
  playground,
  context: req => ({ ...req, db })
};

const server = new ApolloServer(serverConfig);

const app = express();

app.use(cookieParser());
app.use(decodeToken);
app.use(populateUser);

server.applyMiddleware({ app, path: '/' });

export default app;
