import { Prisma } from './generated/prisma-client';

const db = new Prisma({ debug: true });

export default db;
