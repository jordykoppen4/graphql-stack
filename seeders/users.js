const { prisma } = require('../src/generated/prisma-client');

const users = async () => {
  await prisma.createUser({
    email: 'test@example.com',
    name: 'Test User',
    password: 'password'
  });
};

module.exports = users;
