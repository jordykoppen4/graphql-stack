const { prisma } = require('../src/generated/prisma-client');
const faker = require('faker');

const posts = async () => {
  for (let i = 0; i < 10; i++) {
    await prisma.createPost({
      title: faker.lorem.sentence(),
      content: faker.lorem.paragraphs(),
      author: { connect: { email: 'test@example.com' } }
    });
  }
};

module.exports = posts;
