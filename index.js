require('dotenv').config({ path: '.env' });
require('@babel/register')({
  presets: [['@babel/preset-env', { targets: { node: 'current' } }]],
  plugins: [],
});

module.exports = require('./src/index.js');
