# Prisma + Apollo-Server boilerplate

- [Prisma](https://www.prisma.io/docs/)
- [Apollo-Server](https://www.apollographql.com/docs/apollo-server/)

## Getting Started

Install the project's dependencies

```
$ yarn

or

$ npm install
```

Then there are two options: Run a Prisma [demo](https://www.prisma.io/docs/prisma-server/demo-servers-prisma-cloud-jfr3/) or [local](https://www.prisma.io/docs/prisma-server/local-prisma-setup-je3i/) instance. For a demo instance you need to sign up on [pisma.io](https://prisma.io), create a demo instance and save the endpoint in your `.env` file.

For a local instance you need [Docker](https://docker.com). Once Docker is installed run the following command to run the docker containers.

```
$ yarn docker:up

or

$ npm docker:up
```

Then after you set up your local/demo instance, run the deploy command to deploy your datamodel to the prisma instance and generate the `prisma-client`.

```
$ yarn deploy

or

$ npm run deploy
```

That's it, now launch the server

```
$ yarn start

or

$ npm start
```

Prisma's GraphQL playground will be on the endpoint you got from the demo instance or http://localhost:4466 if local.

The Admin panel is accessible trough the prisma demo interface or on local at http://localhost:4466/_admin
